package edu.metrostate.ics340.p5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Random;
import java.util.logging.Logger;

public class P5ScenarioGenerator {
	public final static int NUM_SCENARIOS = 10;
	public final static int MIN_POINTS = 30;
	public final static int MAX_POINTS = 50;

	public static final String FILE_DIRECTORY = "/tmp";
	public final static String FILE_NAME_FORMAT = "%s/p5_scenario_G%02d.txt";
	public final static String DATA_FORMAT = "SCN%02d_P%02d|%7.5f|%7.5f\n";

	public final static long SEED = 20190804001L;
	public final static Random RAND = new Random(SEED);

	public static void main(String[] args) {
		
		for (int scenarioNum = 0; scenarioNum < NUM_SCENARIOS; scenarioNum++) {
			final var fileName = new File(String.format(FILE_NAME_FORMAT, FILE_DIRECTORY, scenarioNum));
			try (PrintStream scenarioFile = new PrintStream(fileName);){
				int numLocations = randBetween(MIN_POINTS, MAX_POINTS);
				for(int i = 0; i < numLocations; i++) {
					scenarioFile.printf(DATA_FORMAT, scenarioNum, i, RAND.nextDouble() * 1000, RAND.nextDouble() * 1000);
				}
				Logger.getAnonymousLogger().info(String.format("%s: %d locations", fileName, numLocations));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}

	private static int randBetween(int min, int max) {
		return min + RAND.nextInt(max - min);
	}

}
