package edu.metrostate.ics340.p5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.rules.Timeout;

import edu.metrostate.ics340.p5.aaa000.AbstractPerimeterFinder;
import edu.metrostate.ics340.p5.aaa000.Location;

//import edu.metrostate.ics340.p5.aaa000.gram.PerimeterFinder;
import edu.metrostate.ics340.p5.aaa000.jarvis.PerimeterFinder;

class PerimeterFinderTest {

	private static final int TIMEOUT_SECONDS = 5;

	private static final String SCENARIOS_DIR = "scenarios/";

	@Rule
	public Timeout globalTimeout = Timeout.seconds(30);

	@BeforeAll
	static void displayTestClass() {
		Logger.getAnonymousLogger().info(String.format("Test class: %s", PerimeterFinder.class.getName()));
	}

	@ParameterizedTest
	@Tag("Functional")
	@MethodSource("scenarios")
	public void functionTests(String scenario, String expResult) {
		// assertTimeoutPreemptively(Duration.ofSeconds(TIMEOUT_SECONDS), ()->
		// test(scenario, expResult));
		test(scenario, expResult);
	}

	@ParameterizedTest
	@Tag("Functional")
	@MethodSource("generatedScenarios")
	public void generatedTests(String scenario) {
		assertTimeoutPreemptively(Duration.ofSeconds(TIMEOUT_SECONDS), () -> test(scenario));
	}

	private void test(String scenario) {
		String fileName = getFilePath(scenario);
		Set<String> expResults = new TreeSet<>(
				edu.metrostate.ics340.p5.aaa000.gram.PerimeterFinder.getBoundary(fileName));
		genHullFile(fileName, expResults);
		var result = new TreeSet<String>(PerimeterFinder.getBoundary(fileName));
		genHullFile(fileName, "-RES", result);

		assertEquals(expResults, result);

	}

	@ParameterizedTest
	@Tag("QA")
	@MethodSource("qaScenarios")
	public void qaTests(String scenario, String expResult) {
		// assertTimeoutPreemptively(Duration.ofSeconds(TIMEOUT_SECONDS), () ->
		// test(scenario, expResult));
		test(scenario, expResult);
	}

	public void test(String scenario, String expResult) {
		String scenarioNum = scenario;
		Set<String> expResults = expResult.isBlank() ? new HashSet<>()
				: new HashSet<>(Arrays.asList(expResult.split("\\W")));

		String fileName = getFilePath(scenarioNum);
		genHullFile(fileName, expResults);

		var result = PerimeterFinder.getBoundary(fileName);
		genHullFile(fileName, "-RES", result);

		assertEquals(expResults, result);
	}

	private void genHullFile(String fileName, Set<String> expResults) {
		genHullFile(fileName, "", expResults);
	}

	private String getFilePath(String scenarioNum) {
		return PerimeterFinderTest.class.getResource(String.format(SCENARIOS_DIR + "p5_scenario%s.txt", scenarioNum))
				.getPath();
	}

	public static String[][] scenarios() {
		return new String[][] { { "0", "A B C" }, { "1", "A B C" }, { "2", "A B E" }, { "3", "A B E F" } };
	}

	public static String[][] qaScenarios() {
		return new String[][] { { "4", "" }, { "5", "" }, { "6", "" }, { "7", "" }, { "8", "A Y Z" },
				{ "9", "A B C" } };
	}

	public static String[] generatedScenarios() {
		List<String> scenarios = new LinkedList<>();

		for (int i = 0; i <= 13; i++) {
			scenarios.add(String.format("_G%02d", i));
		}

		return scenarios.toArray(new String[scenarios.size()]);
	}

	private static void genHullFile(String sectorFile, String suffix, Set<String> hullIDs) {
		var locations = AbstractPerimeterFinder.loadSectorFile(sectorFile);
		var polarLocations = AbstractPerimeterFinder.getPolarLocations(AbstractPerimeterFinder.findLeftmost(locations),
				locations);
		Arrays.sort(polarLocations);
		Map<String, Location> locMap = new HashMap<>();
		for (var location : polarLocations) {
			locMap.put(location.getId(), location);
		}
		String fileName = "/tmp".concat(sectorFile.substring(sectorFile.lastIndexOf('/')).replace(".txt",
				String.format("%s_hull.csv", suffix)));
		try (var hullFile = new PrintWriter(new File(fileName));) {
			hullFile.printf("Location,X,Y\n");
			Location firstPoint = null;
			for (var loc : polarLocations) {
				if (hullIDs.contains(loc.getId())) {
					if (firstPoint == null) {
						firstPoint = loc;
					}
					var location = locMap.get(loc.getId());
					hullFile.printf("%s,%5.3f,%5.3f\n", location.getId(), location.getX(), location.getY());
				}
			}
			if (firstPoint != null)
				hullFile.printf("%s,%5.3f,%5.3f\n", firstPoint.getId(), firstPoint.getX(), firstPoint.getY());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
