package edu.metrostate.ics340.p5.aaa000.gram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import edu.metrostate.ics340.p5.aaa000.AbstractPerimeterFinder;
import edu.metrostate.ics340.p5.aaa000.Location;
import edu.metrostate.ics340.p5.aaa000.PolarLocation;

public class PerimeterFinder extends AbstractPerimeterFinder {
	private static class DoublePeekStack<T> implements Iterable<T>{
		private List<T> items;

		@Override
		public String toString() {
			return String.format("[%s]", items);
		}

		public DoublePeekStack() {
			items = new ArrayList<>();
		}

		public void push(T item) {
			items.add(item);

		}

		public T pop() {
			return items.remove(items.size() - 1);

		}

		public T nextToTop() {
			return items.get(items.size() - 2);
		}

		public T peek() {
			return items.get(items.size() - 1);
		}

		@Override
		public Iterator<T> iterator() {
			return items.iterator();
		}

		public int size() {
			return items.size();
		}

		public PolarLocation[] toArray() {
			return items.toArray(new PolarLocation[items.size()]);
		}
	}
	private static Set<String> findCovexHull(Location[] locations) {
		DoublePeekStack<PolarLocation> s;
		Arrays.sort(locations);
		
		var p0 = locations[0];

		var scanLocations = getPolarLocations(p0, locations);
		Arrays.sort(scanLocations);

		if (scanLocations.length <= 2 || (scanLocations.length == 3 && turnAngle(scanLocations) == Turn.COLINEAR)) {
			return EMPTY_HULL;
		} else {
			s = new DoublePeekStack<>();
			for (int i = 0; i <= 2; i++) {
				s.push(scanLocations[i]);
			}
			for (int i = 3; i < scanLocations.length; i++) {
				if (s.size() < 2) {
					// colinear points: no null
					return EMPTY_HULL;
				}
				while (s.size() >= 2 && turnAngle(s.nextToTop(), s.peek(), scanLocations[i]) != Turn.RIGHT) {
					s.pop();
				}
				s.push(scanLocations[i]);
			}
		}
		return s.size() < 3 || (s.size() == 3 && turnAngle(s.toArray()) == Turn.COLINEAR) ? EMPTY_HULL : getLocationIDs(s);
	}

	private static Set<String> getLocationIDs(DoublePeekStack<PolarLocation> s) {
		Set<String> ids = new HashSet<>();

		for (Location location : s) {
			ids.add(location.getId());
		}
		return ids;
	}

	public static Set<String> getBoundary(String sectorFile) {
		Location[] locations = loadSectorFile(sectorFile);
		return findCovexHull(locations);
	}
}
