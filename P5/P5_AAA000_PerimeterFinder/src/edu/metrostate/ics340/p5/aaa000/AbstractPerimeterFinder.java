package edu.metrostate.ics340.p5.aaa000;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

public class AbstractPerimeterFinder {

	protected enum Turn {
			LEFT, COLINEAR, RIGHT
		}

	protected static final Set<String> EMPTY_HULL = new HashSet<String>();

	public static Location[] loadSectorFile(String sectorFile) {
		List<Location> locations = new LinkedList<>();
	
		try (Scanner input = new Scanner(new File(sectorFile))) {
			input.useDelimiter("[\\|\n\r]+");
			while (input.hasNext()) {
				locations.add(new Location(input.next(), input.nextDouble(), input.nextDouble()));
			}
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
		return locations.toArray(new Location[locations.size()]);
	}

	protected static Turn turnAngle(Location[] locs) {
		return turnAngle(locs[0], locs[1], locs[2]);
	}

	protected static Turn turnAngle(Location p0, Location p1, Location p2) {
		double xProduct = ((p1.getX() - p0.getX()) * (p2.getY() - p0.getY()))
				- ((p2.getX() - p0.getX()) * (p1.getY() - p0.getY()));
		final var turn = xProduct < 0 ? Turn.RIGHT : xProduct > 0 ? Turn.LEFT : Turn.COLINEAR;
		Logger.getAnonymousLogger().info(String.format("%s %s %s : %s", p0.getId(), p1.getId(), p2.getId(), turn));
		return turn;
	}

	public static PolarLocation[] getPolarLocations(Location p0, Location[] locations) {
		PolarLocation[] pLocations = new PolarLocation[locations.length];
		for (int i = 0; i < pLocations.length; i++) {
			pLocations[i] = new PolarLocation(locations[i], p0.toPolar(locations[i]));
		}
		return pLocations;
	}

	public static Location findLeftmost(Location[] locations) {
		Arrays.sort(locations);
		return locations[0];
	}

	public AbstractPerimeterFinder() {
		super();
	}

}