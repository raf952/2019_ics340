package edu.metrostate.ics340.p5.aaa000;

import java.util.Comparator;

public class Location implements Comparable<Location> {
	private static final Comparator<Location> LOCATION_COMPARATOR = Comparator.comparingDouble(Location::getX).thenComparingDouble(Location::getY);
	final String id;
	final private double x;
	final private double y;

	public Location(String id, double x, double y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return String.format("[%s (%5.3f, %5.3f)", id, x, y);
	}

	public Location(Location loc) {
		this(loc.id, loc.x, loc.y);
	}

	public String getId() {
		return id;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public int compareTo(Location o) {
		return LOCATION_COMPARATOR.compare(this, o);
	}

	public PolarCoordinate toPolar(Location point) {
		final var transposedX = point.x - this.x;
		final double transposedY = point.y - this.y;
		double r = Math.sqrt((transposedX * transposedX) + (transposedY * transposedY));
		double theta =  Math.atan2(transposedX, transposedY);
		return this.equals(point) ? new PolarCoordinate(0, 0) : new PolarCoordinate(r, theta);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}


	
}