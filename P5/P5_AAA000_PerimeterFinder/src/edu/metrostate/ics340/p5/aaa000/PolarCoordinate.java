package edu.metrostate.ics340.p5.aaa000;

import java.util.Comparator;

public class PolarCoordinate implements Comparable<PolarCoordinate> {

	public static final Comparator<PolarCoordinate> POLAR_COORDINATE_COMPARATOR = Comparator.comparingDouble(PolarCoordinate::getTheta).thenComparing(PolarCoordinate::getNegR);
	double r;
	double theta;

	public double getR() {
		return r;
	}
	
	public double getNegR() {
		return -r;
	}
	
	public double getTheta() {
		return theta;
	}

	public PolarCoordinate(double r, double theta) {
		this.r = r;
		this.theta = theta;
	}

	@Override
	public int compareTo(PolarCoordinate o) {
		return POLAR_COORDINATE_COMPARATOR.compare(this, o);
	}

}