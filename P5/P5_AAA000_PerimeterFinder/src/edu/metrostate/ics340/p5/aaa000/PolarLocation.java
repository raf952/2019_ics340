package edu.metrostate.ics340.p5.aaa000;

import java.util.Comparator;

public class PolarLocation extends Location {
	public static final Comparator<PolarLocation> POLAR_LOCATION_COMPARATOR = Comparator.comparingDouble(PolarLocation::getTheta).thenComparing(PolarLocation::getR);

	private PolarCoordinate polar;

	public PolarLocation(Location loc, PolarCoordinate polar) {
		super(loc);
		this.polar = polar;
	}

	public double getR() {
		return polar.r;
	}
	
	
	public double getTheta() {
		return polar.theta;
	}
	
	@Override
	public String toString() {
		return String.format("[%s <%5.3f, %5.3f>", getId(), polar.r, polar.theta);
	}

	@Override
	public int compareTo(Location o) {

		return POLAR_LOCATION_COMPARATOR.compare(this, (PolarLocation) o);
	}

}