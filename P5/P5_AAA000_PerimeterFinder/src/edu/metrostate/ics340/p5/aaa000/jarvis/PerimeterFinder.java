package edu.metrostate.ics340.p5.aaa000.jarvis;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.metrostate.ics340.p5.aaa000.AbstractPerimeterFinder;
import edu.metrostate.ics340.p5.aaa000.Location;

public class PerimeterFinder extends AbstractPerimeterFinder {

	private static final Comparator<Location> LEFTMOST_COMPARATOR = java.util.Comparator.comparingDouble(Location::getX)
			.thenComparing(Location::getY);

	public static Set<String> getBoundary(String sectorFile) {
		Location[] locations = loadSectorFile(sectorFile);
		return findCovexHull(locations);
	}

	private static Set<String> findCovexHull(Location[] locations) {
		List<Location> hullPoints = new ArrayList<>();

		hullPoints.add(findLeftmostPoint(locations));

		Location endPoint = locations[0];
		do {
			for (int j = 0; j < locations.length; j++) {
				Location pointOnHull = hullPoints.get(hullPoints.size() - 1);
				if (endPoint.equals(pointOnHull) || turnAngle(pointOnHull, endPoint, locations[j]) == Turn.LEFT) {
					endPoint = locations[j];
				}
			}
			hullPoints.add(endPoint);

		} while (!hullPoints.get(0).equals(endPoint));

		return getLocationIDs(hullPoints);
	}

	private static Location findLeftmostPoint(Location[] locations) {
		Location leftmost = new Location("stub", Double.MAX_VALUE, Double.MAX_VALUE);

		for (var currLocation : locations) {
			if (LEFTMOST_COMPARATOR.compare(currLocation, leftmost) < 0) {
				leftmost = currLocation;
			}
		}
		return leftmost;
	}

	/**
	 * @param hullPoints
	 * @return
	 */
	private static Set<String> getLocationIDs(List<Location> hullPoints) {
		if (hullPoints.size() < 4) {
			return EMPTY_HULL;
		} else {
			Set<String> locNames = new HashSet<>();
			for (var loc : hullPoints) {
				locNames.add(loc.getId());
			}
			return locNames;
		}
	}

}
