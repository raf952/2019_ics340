package edu.metrostate.ics340.fx.aaa000;

import java.util.logging.Logger;

public class PalindromeGenerator {

	private static final int NOT_FOUND = -1;
	private static final int MAX_VAL = 1_000_000_000;

	public static int generate(int n) {
		Logger.getAnonymousLogger().info(String.format("generate(%d)", n));
		if (n < 1) {
			throw new IllegalArgumentException("n must be >= 1");
		} else if (isPalindrome(n))
			return n;
//		else if (n  > MAX_VAL)
			else if (MAX_VAL - n < reverse(n))
			return NOT_FOUND;
		else
			return generate(n + reverse(n));
	}

	private static boolean isPalindrome(int n) {
		return n == reverse(n);
	}

	private static int reverse(int n) {
		StringBuilder sb = new StringBuilder(Integer.toString(n));
		sb.reverse();
		return Integer.valueOf(sb.toString());
	}
}
