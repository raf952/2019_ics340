package edu.metrostate.ics340.fx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Random;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import edu.metrostate.ics340.fx.aaa000.PalindromeGenerator;


class PalindromeGeneratorTest {

	private static final int NUM_TESTS = 100;
	private static final long SEED = 20190814001L;
	private static final Random RAND = new Random(SEED);

	@BeforeAll
	static void displayTestClass() {
		System.out.println("Class Tested: " + PalindromeGenerator.class.getName());
	}
	
	@ParameterizedTest
	@Tag("Functional")
	@MethodSource("funcScenarios")
	public void test(Integer n, Integer palN) {
		assertEquals(palN, PalindromeGenerator.generate(n), String.format("n = %d", n));
	}
	
	@Test
	@Tag("Functional")
	public void testRandom() {
		for (int i = 0; i < NUM_TESTS; i++) {
			int n = randBetween(1, 10_000);
			assertEquals(edu.metrostate.ics340.fx.aaa000.PalindromeGenerator.generate(n),
					PalindromeGenerator.generate(n)
					,String.format("n = %d", n));
		}
	}

	private int randBetween(int min, int max) {
		return min + RAND.nextInt(max - min);
	}

	@Test
	@Tag("QA")
	public void testNeg() {
		assertThrows(IllegalArgumentException.class, ()-> PalindromeGenerator.generate(-1));
	}

	@Test
	@Tag("QA")
	public void test0() {
		assertThrows(IllegalArgumentException.class, ()-> PalindromeGenerator.generate(0));
	}
	
	public static Stream<Arguments> funcScenarios(){	
		
		return Stream.of(Arguments.of(1, 1),
				Arguments.of(9, 9),
				Arguments.of(10, 11),
				Arguments.of(19, 121),
				Arguments.of(49, 484),
				Arguments.of(79, 44044),
				Arguments.of(196, -1)
				);
	}
}
