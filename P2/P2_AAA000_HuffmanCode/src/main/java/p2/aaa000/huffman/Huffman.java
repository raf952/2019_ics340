package p2.aaa000.huffman;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.math3.util.IntegerSequence.Incrementor;

/**
 * @author rfoy
 *
 */
public class Huffman {
	private static final Logger LOGGER = Logger.getAnonymousLogger();
	private Map<Character, String> encodingMap;
	private HuffmanNode root;

	private Huffman(Map<Character, Double> frequencyMap) {
		root = buildTree(frequencyMap);
		encodingMap = buildEncodingMap(root);
	}

	public static Huffman build(String filename) {
		var frequencyMap = calculateFrequencies(filename);
		return new Huffman(frequencyMap);
	}

	public static Map<Character, Double> calculateFrequencies(String fileName) {
		Map<Character, Incrementor> counterMap = new HashMap<Character, Incrementor>();
		int charCount = 0;
		// InputStream inputFile = HuffmanCode.class.getResourceAsStream(fileName);
		try (Reader input = new InputStreamReader(new FileInputStream(fileName));) {
			int r;
			while ((r = input.read()) != -1) {
				Character c = Character.valueOf(Character.toUpperCase((char) r));
				charCount++;
				if (counterMap.containsKey(c)) {
					counterMap.get(c).increment();
				} else {
					counterMap.put(c, Incrementor.create().withStart(1).withMaximalCount(Integer.MAX_VALUE));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<Character, Double> frequencyMap = new HashMap<Character, Double>();
		for (Map.Entry<Character, Incrementor> entry : counterMap.entrySet()) {
			frequencyMap.put(entry.getKey(), ((double) entry.getValue().getCount() / charCount));
		}
		return frequencyMap;
	}

	private static HuffmanNode buildTree(Map<Character, Double> frequencies) {
		// add entries to priority queue
		PriorityQueue<HuffmanNode> huffQueue = new PriorityQueue<>();
		for (Map.Entry<Character, Double> entry : frequencies.entrySet()) {
			huffQueue.add(new HuffmanNode(entry.getKey(), entry.getValue()));
		}
		// while queue.size > 1
		// -- remove 2 nodes
		// -- add combined nodes
		while (huffQueue.size() > 1) {
			huffQueue.add(new HuffmanNode(huffQueue.remove(), huffQueue.remove()));
		}
		// return remaining node
		return huffQueue.remove();
	}

	public static void main(String[] args) {

		var hc = Huffman.build(Huffman.class.getResource("input/loremIpsum.txt").getPath());
		var msg = hc.encode(Huffman.class.getResource("input/lorem2.txt").getPath());

		try (OutputStream out = new FileOutputStream("/tmp/lorem.dat");) {
			out.write(msg.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		var textMsg = hc.decode(msg);
		System.out.println(textMsg);

	}

	public String decode(String msg) {
		StringBuffer text = new StringBuffer();
		var currNode = root;
		StringBuffer code = new StringBuffer();
		for (var bit : msg.toCharArray()) {
			code.append(bit);
			currNode = bit == '0' ? currNode.getLeftChild() : currNode.getRightChild();
		
			if (currNode.getValue() != Character.MIN_VALUE) {
				text.append(currNode.getValue());
				currNode = root;
				code.setLength(0);
			}
		}
		if (currNode.getValue() == Character.MIN_VALUE) {
			throw new IllegalArgumentException(String.format("Invalid code %s", code));
		}
		return text.toString();
	}

	public String encode(String path) {
		StringBuffer sb = new StringBuffer();

		try (Scanner input = new Scanner(path)) {
			input.useDelimiter("");
			while (input.hasNext()) {
				Character currChar = input.next().toUpperCase().charAt(0);
				
				String code = encodingMap.get(currChar);
				if (code == null ) {
					throw new IllegalArgumentException(String.format("Invalid code %s", code));
				}
				sb.append(code);
			}
		}

	
		return sb.toString();
	}

	public Map<Character, String> getEncodingMap() {
		return new HashMap<>(encodingMap);
	}

	private static Map<Character, String> buildEncodingMap(HuffmanNode root) {
		HashMap<Character, String> encodingMap = new HashMap<Character, String>();
		buildEncodingMap(root, encodingMap, "");
		return encodingMap;
	}

	private static void buildEncodingMap(HuffmanNode node, Map<Character, String> encodingMap, String code) {
		if (node == null) {
			return;
		} else {
			if (!node.getValue().equals(Character.MIN_VALUE)) {
				encodingMap.put(node.getValue(), code);
			}
			buildEncodingMap(node.getLeftChild(), encodingMap, code + "0");
			buildEncodingMap(node.getRightChild(), encodingMap, code + "1");
		}
	}

	public HuffmanNode getDecodingTree() {
		return root;
	}
}
