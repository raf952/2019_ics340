package p2.aaa000.huffman;

import java.util.Comparator;

import edu.metrostate.ics340.p2.TreeNode;

/**
 * Node for Huffman Coding Tree
 * @author rfoy
 *
 */
public class HuffmanNode implements TreeNode<Character>, Comparable<HuffmanNode> {
	private static final Comparator<HuffmanNode> NODE_COMPARATOR = Comparator.comparing(HuffmanNode::getFreq)
			.thenComparing(HuffmanNode::getValue);
	private Character c;
	private Double freq;
	private HuffmanNode left;
	private HuffmanNode right;

	/**
	 * Node of this Huffman Tree
	 * @param c
	 * @param freq
	 */
	public HuffmanNode(Character c, double freq) {
		this.c = c;
		this.freq = freq;
	}

	public HuffmanNode(HuffmanNode rChild, HuffmanNode lChild) {
		c = Character.MIN_VALUE;
		this.freq = lChild.getFreq() + rChild.getFreq();
		left = lChild;
		right = rChild;
	}

	public HuffmanNode getLeftChild() {
		return left;
	}

	public HuffmanNode getRightChild() {
		return right;
	}

	public Character getValue() {
		return c;
	}

	public Double getFreq() {
		return freq;
	}

	@Override
	public int compareTo(HuffmanNode o) {
		return NODE_COMPARATOR.compare(this, o);
	}
}