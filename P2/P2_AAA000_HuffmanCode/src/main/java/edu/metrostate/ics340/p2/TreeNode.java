package edu.metrostate.ics340.p2;

public interface TreeNode<T> {
	TreeNode<T> getLeftChild();
	TreeNode<T> getRightChild();
	T getValue();
}
