package p2.huffman;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.logging.Logger;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import p2.aaa000.huffman.Huffman;

public class HuffmanCodeTest {

	@Test
	@Tag("Functional")
	void test() {
		var hc = Huffman.build(HuffmanCodeTest.class.getResource("input/lipsum.txt").getPath());

		assertNotNull(hc);
		final var encodingMap = hc.getEncodingMap();
		assertNotNull(encodingMap);
		assertNotNull(hc.getDecodingTree());

		for (var entry : encodingMap.entrySet()) {
			assertEquals(entry.getKey().toString().toUpperCase(), hc.decode(entry.getValue()).toUpperCase());
		}

		String msg = "this is a test.";
		String code = hc.encode(msg);
		Logger.getAnonymousLogger().info(String.format("%s > %s", msg, code));

		String decode = hc.decode(code);
		assertEquals(msg.toUpperCase(), decode);
		Logger.getAnonymousLogger().info(String.format("decode: %s", decode));
	}

	@Test
	@Tag("QA")
	void testEncodeError() {
		var hc = Huffman.build(HuffmanCodeTest.class.getResource("input/lipsum.txt").getPath());

		assertNotNull(hc);
		assertNotNull(hc.getEncodingMap());
		assertNotNull(hc.getDecodingTree());

		String msg = "this isn' a test.";
		assertThrows(IllegalArgumentException.class, () -> hc.encode(msg));
	}

	@Test
	@Tag("QA")
	void testDecodeError() {
		var hc = Huffman.build(HuffmanCodeTest.class.getResource("input/lipsum.txt").getPath());
		assertThrows(IllegalArgumentException.class, () -> hc.decode("01"));
	}
}
