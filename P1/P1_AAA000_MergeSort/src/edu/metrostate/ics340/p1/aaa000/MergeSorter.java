package edu.metrostate.ics340.p1.aaa000;

import java.util.Arrays;

/**
 * @author rfoy
 *
 */
public class MergeSorter {

	private static void merge(int[] a, int p, int q, int r) {
		int n1 = q - p + 2;
		int n2 = r - q + 1;
		int[] left = new int[n1];
		Arrays.copyOfRange(a, p, q);
		int[] right = new int[n2];
		Arrays.copyOfRange(a, q + 1, r);

		if (p < r) {
			copyInto(left, a, p, q);
			copyInto(right, a, q + 1, r);

			int i = 0;
			int j = 0;
			for (int k = p; k <= r; k++) {
				a[k] = (left[i] <= right[j]) ? left[i++] : right[j++];
			}
		}
	}

	private static void merge(int[] a, int p, int r) {
		if (p < r) {
			int q = (p + r) / 2;
			merge(a, p, q);
			merge(a, q + 1, r);
			merge(a, p, q, r);
		}
	}

	/**
	 * @param a
	 */
	public static void sort(int[] a) {
		merge(a, 0, a.length - 1);
	}

	private static void copyInto(int[] dest, int[] src, int start, int end) {
		for (int i = 0; i <= end - start; i++) {
			dest[i] = src[start + i];
		}
		dest[dest.length - 1] = Integer.MAX_VALUE;
	}
}
