package edu.metrostate.ics340.p1.aaa000.generic;

import java.util.Arrays;

/**
 * @author rfoy Copyright 2019. Ralph A. Foy. All rights reserved.
 */
public class MergeSorter {

	@SuppressWarnings("unchecked")
	private static <T> void merge(Comparable<T>[] items, int start, int middle, int length) {

		if (start < length) {
			var left = Arrays.copyOfRange(items, start, middle + 1);
			var right = Arrays.copyOfRange(items, middle + 1, length + 1);

			int i = 0;
			int j = 0;
			for (int k = start; k <= length; k++) {
				items[k] = (i < left.length // there are more items on the left
						&& (j >= right.length // there are no more items on the right
								|| (left[i]).compareTo((T) right[j]) < 0)) // the left item is less than the right
										? left[i++] // return the left item
										: right[j++]; // return the right item
			}
		}
	}

	private static <T> void mergeSort(Comparable<T>[] items, int start, int length) {
		if (start < length) {
			int middle = (start + length) / 2;
			mergeSort(items, start, middle);
			mergeSort(items, middle + 1, length);
			merge(items, start, middle, length);
		}
	}

	/**
	 * Sort based on the Mergesort algorithm in Cormen (2009)
	 * 
	 * @param <T> type of items to be sorted. must be <code>Comparable</code>
	 * @param items   array of items to be sorted. Cannot be null
	 */
	public static <T> void sort(Comparable<? extends T>[] items) {
		if (items == null) {
			throw new NullPointerException("Array cannot be null");
		} else {
			mergeSort(items, 0, items.length - 1);
		}
	}
}
