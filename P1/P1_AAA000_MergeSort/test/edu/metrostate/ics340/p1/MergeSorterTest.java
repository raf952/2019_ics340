package edu.metrostate.ics340.p1;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import org.apache.commons.text.RandomStringGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import edu.metrostate.ics340.p1.aaa000.generic.MergeSorter;

/**
 * @author rfoy
 *
 */
@SuppressWarnings("static-method")
public class MergeSorterTest {
	/**
	 * 
	 */
	private static final Logger LOGGER = Logger.getAnonymousLogger();
	private final static long SEED = 20190429001L;
	private final static Random RAND = new Random(SEED);

	/**
	 * 
	 */
	@Test
	@Tag("QA")
	public void testNull() {
		assertThrows(NullPointerException.class, () -> MergeSorter.sort(null));
	}

	/**
	 * 
	 */
	@Test
	@Tag("Functional")
	public void test0() {
		Boolean[] arr = new Boolean[] {};
		MergeSorter.sort(arr);
		assertEquals(0, arr.length);
	}

	/**
	 * 
	 */
	@Test
	@Tag("Functional")
	public void test1() {
		Long[] arr = new Long[] { 20190526001L };
		MergeSorter.sort(arr);
		assertEquals(1, arr.length);
	}

	/**
	 * 
	 */
	@Test
	@Tag("Functional")
	public void testInts() {
		int n = randBetween(1000, 10_000);
		LOGGER.info(String.format("n = %d\n", n));

		var vals = new Integer[n];
		for (int i = 0; i < vals.length; i++) {
			vals[i] = RAND.nextInt();
		}

		var exp = vals.clone();
		testMergeSort(vals, exp);
	}

	/**
	 * 
	 */
	@Test
	@Tag("Functional")
	public void bookExample() {
		Integer[] nums = new Integer[] { 5, 2, 4, 7, 1, 3, 2, 6 };
		var exp = nums.clone();
		var orig = nums.clone();

		testMergeSort(nums, exp);
		LOGGER.info(String.format("%s: %s >> %s\n", MergeSorter.class.getCanonicalName(), Arrays.toString(orig),
				Arrays.toString(nums)));
	}

	/**
	 * @param values
	 * @param exp
	 */
	private static <T extends Comparable<T>> void testMergeSort(T[] values, Comparable<? extends T>[] exp) {
		Arrays.sort(exp);
		MergeSorter.sort(values);
		assertArrayEquals(exp, values);
	}

	/**
	 * 
	 */
	@Test
	@Tag("Functional")
	public void testGenericStrings() {
		int n = randBetween(1000, 10_000);
		LOGGER.info(String.format("n = %d\n", n));
		String[] vals = new String[n];
		RandomStringGenerator strGen = new RandomStringGenerator.Builder().build();
		for (int i = 0; i < vals.length; i++) {
			vals[i] = strGen.generate(8);
		}

		String[] exp = vals.clone();
		Arrays.parallelSort(exp);
		MergeSorter.sort(vals);
		assertArrayEquals(exp, vals);
	}

	private static int randBetween(int min, int max) {
		return min + RAND.nextInt(max - min);
	}

}
