package mt;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import mt.aaa000.MoneyDispenser;
class MoneyDispenserTest {
	final static int [] US_CURRENCY = {1, 5, 10, 20, 50, 100};

	final static int [] XYZZY_CURRENCY = {1, 4, 9, 19, 49, 99, 499};
	
	final static int [] IN_CURRENCY = {1};
	
	final static int [] DEMO_CURRENCY = {1, 3, 4};
	
	@BeforeAll
	public static void displayClassInfo() {
		System.out.println("Class Tested: " + MoneyDispenser.class.getTypeName());
	}
	
	@Test
	void test0X() {
		var md = new MoneyDispenser(XYZZY_CURRENCY);
		assertArrayEquals(new int [] {0, 0, 0, 0, 0, 0, 0}, md.dispense(0));
	}
	
	@Test
	void test1X() {
		var md = new MoneyDispenser(XYZZY_CURRENCY);
		assertArrayEquals(new int [] {1, 0, 0, 0, 0, 0, 0}, md.dispense(1));
	}	
	
	@Test
	void testAllX() {
		var md = new MoneyDispenser(XYZZY_CURRENCY);
		assertArrayEquals(new int [] {1, 1, 1, 1, 1, 1, 1}, md.dispense(680));
	}	
	@Test
	void test0() {
		var md = new MoneyDispenser(US_CURRENCY);
		assertArrayEquals(new int [] {0, 0, 0, 0, 0, 0}, md.dispense(0));
	}

	@Test
	void test1() {
		var md = new MoneyDispenser(US_CURRENCY);
		assertArrayEquals(new int [] {1, 0, 0, 0, 0, 0}, md.dispense(1));
	}


	@Test
	void testNeg1() {
		var md = new MoneyDispenser(US_CURRENCY);
		assertThrows(IllegalArgumentException.class, ()->md.dispense(-1));
	}

	@Test
	void test1234() {
		var md = new MoneyDispenser(US_CURRENCY);
		assertArrayEquals(new int [] {4, 0, 1, 1, 0, 12}, md.dispense(1234));
	}
	@Test
	void test186() {
		var md = new MoneyDispenser(US_CURRENCY);
		assertArrayEquals(new int [] {1, 1, 1, 1, 1, 1}, md.dispense(186));
	}
	
	@Test
	void testPrecondition() {
		var md = new MoneyDispenser(US_CURRENCY);

		assertThrows(IllegalArgumentException.class, ()->md.dispense(-1));
	}
}
