package mt.aaa000;

public class MoneyDispenser {
	private int[] currency;

	public MoneyDispenser(int[] denominations) {
		this.currency = denominations.clone();
	}

	/**
	 * Returns an array of currencies used by this MoneyDispenser
	 * 
	 * @return array of currency denominations
	 */
	public int[] getCurrencies() {
		return currency.clone();
	}

	public int[] dispense(int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException("Amount cannot be less than zero");
		}
		var result = new int[currency.length];

		return dispense(result, amount, currency.length - 1);
	}

	private int[] dispense(int[] result, int amount, int i) {
		if (i < 0) {
			return result;
		} else {
			result[i] = amount / currency[i];
			return dispense(result, amount % currency[i], i - 1);
		}
	}
}
