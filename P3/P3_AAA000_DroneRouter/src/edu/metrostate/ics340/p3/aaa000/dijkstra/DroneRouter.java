package edu.metrostate.ics340.p3.aaa000.dijkstra;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import edu.metrostate.ics340.p3.Router;

/**
 * Solution of shortest path problem using Dijkstra's Algorithm
 * 
 * @author rfoy
 *
 */
public class DroneRouter implements Router {

	private static final String[] EMPTY_PATH = new String[0];

	private static class SourcePath implements Comparable<SourcePath> {
		private static final Comparator<SourcePath> SOURCE_PATH_COMPARATOR = Comparator
				.comparingInt(SourcePath::getValue).thenComparing(SourcePath::getNode);

		public String getNode() {
			return node;
		}

		public int getValue() {
			return value;
		}

		private String node;
		private int value;
		private List<String> path;

		public SourcePath(String node, int value) {
			this.node = node;
			this.path = new LinkedList<>();
			this.value = value;
		}

		@Override
		public String toString() {
			return String.format("%s [%s : %d]", node, path.toString(), value);
		}

		@Override
		public int compareTo(SourcePath o) {
			return SOURCE_PATH_COMPARATOR.compare(this, o);
		}
	}

	private ValueGraph<String, Integer> routes;

	private Map<String, SourcePath> bestRoutes;

	private Map<String, SourcePath> calculateShortestRoutes(String source) {
		Map<String, SourcePath> tracker = new HashMap<>();
		var<String, SourcePath> bestRoutes = new HashMap<String, SourcePath>();

		initialize(tracker, source);
		PriorityQueue<SourcePath> nodeQueue = new PriorityQueue<>();

		nodeQueue.addAll(tracker.values());
		while (!nodeQueue.isEmpty()) {
			Logger.getAnonymousLogger().info(String.format("Queue: %s", nodeQueue));
			Logger.getAnonymousLogger().info(String.format("Nodes: %s", bestRoutes.keySet()));

			var u = nodeQueue.remove();
			bestRoutes.put(u.node, u);
			for (var adjNode : routes.successors(u.node)) {
				if (!bestRoutes.keySet().contains(adjNode)) {
					var adj = tracker.get(adjNode);
					if (adj != null) {
						nodeQueue.remove(adj);
						relax(u, adj);
						nodeQueue.add(adj);
					}
				}
			}
		}

		return bestRoutes;
	}

	private void relax(SourcePath u, SourcePath v) {
		int uvEdgeValue = routes.edgeValue(u.node, v.node).get().intValue();
		if (v.value - uvEdgeValue > u.value) {
			v.path.clear();
			v.path.addAll(u.path);
			v.path.add(u.node);
			v.value = u.value + uvEdgeValue;
		}
	}

	@Override
	public int getPathCost(String destination) {
		if (!bestRoutes.containsKey(destination)) {
			throw new IllegalArgumentException(String.format("Invalid waypoint %s", destination));
		}
		final var route = bestRoutes.get(destination);
		return route.value == Integer.MAX_VALUE ? NO_ROUTE : route.value;
	}

	@Override
	public String[] getRoute(String destination) {
		if (!bestRoutes.containsKey(destination)) {
			throw new IllegalArgumentException(String.format("Invalid waypoint %s", destination));
		}
		var cost = bestRoutes.get(destination).value;
		if (cost == Integer.MAX_VALUE) {
			return EMPTY_PATH;
		} else {
			List<String> path = bestRoutes.get(destination).path;
			path.add(destination);
			return path.toArray(new String[path.size()]);
		}
	}

	private void initialize(Map<String, SourcePath> tracker, String source) {
		for (var node : routes.nodes()) {
			tracker.put(node, new SourcePath(node, node.equals(source) ? 0 : Integer.MAX_VALUE));
		}
	}

	@Override
	public void loadRoutes(String filePath, String source) {
		MutableValueGraph<String, Integer> routes = ValueGraphBuilder.directed().build();
		try (Scanner routeFile = new Scanner(new File(filePath))) {
			while (routeFile.hasNextLine()) {
				routes.putEdgeValue(routeFile.next(), routeFile.next(), routeFile.nextInt());
			}
			this.routes = ImmutableValueGraph.copyOf(routes);
			bestRoutes = calculateShortestRoutes(source);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException(String.format("Unable to open file %s", filePath), e);
		}
	}
}
