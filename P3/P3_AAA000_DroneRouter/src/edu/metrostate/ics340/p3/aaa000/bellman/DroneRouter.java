package edu.metrostate.ics340.p3.aaa000.bellman;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import edu.metrostate.ics340.p3.Router;

public class DroneRouter implements Router {

	private ValueGraph<String, Integer> routes;
	private Map<String, ImmutablePair<List<String>, Integer>> bestRoutes;

	@Override
	public void loadRoutes(String filePath, String source) {
		MutableValueGraph<String, Integer> routes = ValueGraphBuilder.directed().build();
		try (Scanner routeFile = new Scanner(new File(filePath))) {
			while (routeFile.hasNextLine()) {
				routes.putEdgeValue(routeFile.next(), routeFile.next(), routeFile.nextInt());
			}

			this.routes = ImmutableValueGraph.copyOf(routes);
			bestRoutes = calculateShortestRoutes(source, routes);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException(String.format("Unable to open file %s", filePath), e);
		}
	}

	private static class SourcePath {
		public SourcePath() {
			this.path = new LinkedList<>();
			this.value = Integer.MAX_VALUE;
		}

		@Override
		public String toString() {
			return String.format("[%s : %d]", path.toString(), value);
		}

		private int value;
		private List<String> path;
	}

	private Map<String, ImmutablePair<List<String>, Integer>> calculateShortestRoutes(String source,
			MutableValueGraph<String, Integer> routes) {
		Map<String, SourcePath> tracker = new HashMap<>();
		var<String, ImmutablePair<List<String>, Integer>> bestRoutes = new HashMap<String, ImmutablePair<List<String>, Integer>>();

		initialize(tracker);
		tracker.get(source).value = 0;

		calculateIntermediateSolutions(routes, tracker);
		extractFinalSolutions(source, routes, tracker, bestRoutes);
		Logger.getAnonymousLogger().info(String.format("map: %s", tracker.toString()));
		return bestRoutes;
	}

	private static void extractFinalSolutions(String source, MutableValueGraph<String, Integer> routes,
			Map<String, SourcePath> tracker, Map<String, ImmutablePair<List<String>, Integer>> bestRoutes) {
		for (var node : routes.nodes()) {

			final var<String> path = tracker.get(node).path;
			if (path.isEmpty() || !path.get(0).equals(source)) {

				if (node.equals(source)) {
					path.add(source);
					bestRoutes.put(node, new ImmutablePair<>(path, 0));
				} else {
					bestRoutes.put(node, new ImmutablePair<>(path, NO_ROUTE));
				}
			} else {
				path.add(node);
				bestRoutes.put(node, new ImmutablePair<>(path, tracker.get(node).value));
			}
		}
	}

	private void calculateIntermediateSolutions(MutableValueGraph<String, Integer> routes,
			Map<String, SourcePath> tracker) {
		for (int i = 1; i < routes.nodes().size(); i++) {
			Logger.getAnonymousLogger().info(String.format("Tracker: %s", tracker.toString()));
			for (var edge : routes.edges()) {
				var u = edge.nodeU();
				var v = edge.nodeV();
				int uValue = tracker.get(u).value;
				int edgeValue = routes.edgeValue(u, v).get().intValue();
				int vValue = tracker.get(v).value;
				if (routes.hasEdgeConnecting(u, v) && vValue - edgeValue > uValue) {
					var update = tracker.get(v);
					update.path.clear();
					update.path.addAll(tracker.get(u).path);
					update.path.add(u);
					update.value = (uValue == Integer.MAX_VALUE ? 0 : uValue) + edgeValue;
				}
			}
		}
	}

	private void initialize(Map<String, SourcePath> tracker) {
		for (var node : routes.nodes()) {
			tracker.put(node, new SourcePath());
		}
	}

	public ValueGraph<String, Integer> getRoutes() {
		return routes;
	}

	@Override
	public String[] getRoute(String destination) {

		final var routeInfo = bestRoutes.get(destination);
		if (routeInfo == null) {
			throw new IllegalArgumentException(String.format("destination %s does not exist", destination));
		}
		final var route = routeInfo.left;
		return route == null ? null : route.toArray(new String[route.size()]);
	}

	@Override
	public int getPathCost(String destination) {
		final var value = bestRoutes.get(destination).right;
		return value == null ? NO_ROUTE : value.intValue();
	}
}
