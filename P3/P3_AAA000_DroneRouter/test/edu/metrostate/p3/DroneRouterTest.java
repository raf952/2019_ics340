package edu.metrostate.p3;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import edu.metrostate.p3.ooo902.DroneRouter;



//import edu.metrostate.ics340.p3.aaa000.dijkstra.DroneRouter;
//import edu.metrostate.ics340.p3.aaa000.bellman.DroneRouter;
public class DroneRouterTest {

	
	private edu.metrostate.p3.Router router;
//	private edu.metrostate.ics340.p3.Router router;
	private int scenarioNum;
	

	

	@BeforeEach
	void setup() {
		router = new DroneRouter();
	}

	@Test
	void testParam() {
		System.out.println("Scenario #" + scenarioNum);
	}
	
	@Test
	@Tag("ADHOC")
	void test2A_L() {
		checkRoute(0, "A", "I", "A B G", 17);
	}

	@Test
	@Tag("Functional")
	void test0A_G() {
		checkRoute(0, "A", "G", "A B G", 17);
	}

	private void loadRoutes(int i, String source) {
		router.loadRoutes(DroneRouterTest.class.getResource(String.format("input/routes%d.txt", i)).getPath(), source);
	}

	@Test
	@Tag("QA")
	void test0A_Z() {
		loadRoutes(0, "A");
		assertThrows(IllegalArgumentException.class, () -> router.getRoute("Z"));
	}

	@Test
	@Tag("Functional")
	void test0A_M() {
		checkRoute(0, "A", "M", null, Router.NO_ROUTE);
	}

	@Test
	@Tag("Functional")
	void test0B_G() {
		checkRoute(0, "B", "G", "B G", 12);
	}

	@Test
	@Tag("Functional")
	void test0B_M() {
		checkRoute(0, "B", "M", null, Router.NO_ROUTE);
	}

	@Test
	@Tag("Functional")
	void test1L_G() {
		checkRoute(1, "L", "G", "L A B G", 42);
	}

	@Test
	@Tag("Functional")
	void test1L_M() {
		checkRoute(1, "L", "M", "L M", 10);
	}

	@Test
	@Tag("Functional")
	void test1B_A() {
		checkRoute(1, "B", "A", "B C E F L A", 59);
	}

	@Test
	@Tag("Functional")
	void test3_All() {
		checkRoute(3, "A", "B", "A B", 5);
		checkRoute(-3, "A", "C", "A B C", 6);
		checkRoute(-3, "A", "D", "A B C D", 8);
		checkRoute(-3, "A", "A", "A", 0);
	}

	private final static String[] EMPTY_PATH = {};

	private void checkRoute(int scenarioNum, String startWaypoint, String destination, String path, int cost) {
		String[] expPath = path == null ? EMPTY_PATH : path.split("\\s");

		if (scenarioNum >= 0) {
			loadRoutes(scenarioNum, startWaypoint);
		}
		String[] route = router.getRoute(destination);
		Logger.getAnonymousLogger().info(String.format("Result: %s", Arrays.toString(route)));
		assertArrayEquals(expPath, route,
				String.format("Sub-optimal Route: %s (%s)", Arrays.toString(route), Arrays.toString(expPath)));
		assertEquals(cost, router.getPathCost(destination), String.format("Route: %s", Arrays.toString(route)));
	}

	@Test
	@Tag("QA")
	void test1B_Z() {
		loadRoutes(1, "B");
		assertThrows(IllegalArgumentException.class, () -> router.getRoute("Z"));
	}

	@Test
	@Tag("Functional")
	void test1B_M() {
		checkRoute(1, "B", "M", "B C E F L M", 44);
	}

	@Test
	@Tag("QA")
	void badStart() {
		assertThrows(IllegalArgumentException.class, () -> loadRoutes(0, "Z"));
	}

	@Test
	@Tag("QA")
	void badDestination() {
		loadRoutes(0, "A");
		assertThrows(IllegalArgumentException.class, () -> router.getRoute("Z"));
	}

}
