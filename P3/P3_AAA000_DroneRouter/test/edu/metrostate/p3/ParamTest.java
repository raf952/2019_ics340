package edu.metrostate.p3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author rfoy
 *
 */

public class ParamTest {
	private final static int [] scenarios = IntStream.range(1, 10).toArray();


	/**
	 * @param scenario
	 */
	static IntStream numProvider(){
		return IntStream.range(1, 10);
	}


	/**
	 * blah
	 */
	@ParameterizedTest
	@MethodSource("numProvider")
	public  void test(int scenario) {
		System.out.println("Scenario #" + scenario);
		assertEquals(1, 1);
	}
}
