package p4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.logging.Logger;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.graph.ValueGraph;

import edu.metrostate.ics340.p4.ooo902.RailPlanner;














@SuppressWarnings("static-method")
class RailPlannerEval {
	@BeforeAll
	private static void displayClassName() {
		Logger.getAnonymousLogger().info(String.format("Class tested: %s", RailPlanner.class.getName()));
	}

	private static String getFilePath(int scenarioNum) {
		return RailPlannerEval.class.getResource(String.format("data/p4_scenario_F%02d.csv", scenarioNum)).getPath();
	}

	@ParameterizedTest
	@MethodSource("getScenarios")
	@Tag("Functional")
	void testFunctional(int scenarioNum) {
		var scenario = getFilePath(scenarioNum);
		compareResults(scenario);
	}

	private static void compareResults(String scenario) {
		compareResults(scenario, null);
	}

	@Test
	@Tag("QA")
	void testQuality() {
		var testPlanner = new RailPlanner();
		for (int scenarioNum = 7; scenarioNum <= 9; scenarioNum++) {
			var scenario = getFilePath(scenarioNum);
			compareResults(scenario, testPlanner);
		}
	}

	public static int[] getScenarios() {
		return IntStream.range(0, 13).toArray();
	}

	private static void compareResults(String scenario, RailPlanner planner) {
		var referenceResult = p4.aaa000.boruvka.RailPlanner.createPlan(scenario);
		var evalResult = (planner == null ? new RailPlanner() : planner).createPlan(scenario);
		Logger.getAnonymousLogger().info(String.format("Reference: %s\nResult: %s", referenceResult, evalResult));

		
		assertEquals(referenceResult.nodes().size(), evalResult.nodes().size(), "Result missing nodes");
		assertEquals(evalResult.nodes().size() - 1, evalResult.edges().size(),  "Incomplete spanning tree");
		
		assertEquals(getCost(referenceResult), getCost(evalResult));
	}

	private static int getCost(ValueGraph<String, Integer> railPlan) {
		int totalCost = 0;
		for (var edge : railPlan.edges()) {
			totalCost += railPlan.edgeValue(edge).get();
		}
		return totalCost;
	}
}
