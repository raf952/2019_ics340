package p4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.common.graph.ValueGraph;

import edu.metrostate.ics340.p4.msr812.RailPlanner;

 
class RailPlannerTest {

	@BeforeAll
	private static void displayClassName() {
		Logger.getAnonymousLogger().info(String.format("Class tested: %s", RailPlanner.class.getName()));
	}
	
	
	private static String getFilePath(String scenarioNum) {
		return RailPlannerTest.class.getResource(String.format("data/p4_scenario%s.csv", scenarioNum)).getPath();
	}
	
	@Test
	void testLoad00() {
		compareResults(65, "00");
	}


	private void compareResults(int expectedCost, String scenarioSuffix) {
		RailPlanner rp = new RailPlanner();
		assertEquals(expectedCost, getCost(rp.createPlan(getFilePath(scenarioSuffix))));
	}
	
	@Test
	void testLoad5() {
		compareResults(6, "5");
	}
	
	@Test
	void testLoad4() {
		compareResults(6, "4");
	}
	
	@Test
	void testLoad0() {
		compareResults(353, "0");
	}
	
	@Test
	void testLoad1() {
		compareResults(37, "1");
	}
	
	@Test
	void testLoad10() {
		compareResults(2, "10");
	}
	
	@Test
	void testLoad7_9() {
		for (int i = 7; i <= 9; i++) {
			compareResults(3, Integer.toString(i));
		}
	}

	@Test
	void testLoad2() {
		compareResults(40, "2");
	}

	private static int getCost(ValueGraph<String, Integer> railPlan) {
		int totalCost = 0;
		for(var edge : railPlan.edges()) {
			totalCost += railPlan.edgeValue(edge).get();
		}
		return totalCost;
	}
}
