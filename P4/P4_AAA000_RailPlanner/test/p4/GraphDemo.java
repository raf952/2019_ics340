package p4;

import com.google.common.graph.Graphs;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

public class GraphDemo {
	public static void main(String[] args) {
		MutableValueGraph<Character, Integer> g = ValueGraphBuilder.undirected().build();
		g.putEdgeValue('A', 'B', 5);
		g.putEdgeValue('C', 'D', 10);
//		g.putEdgeValue('C', 'B', 15);
		Character aNode = g.nodes().toArray(new Character[0])[0];
		System.out.println(Graphs.reachableNodes(g.asGraph(), aNode));
	}
}
