package p4.aaa000.prim;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Logger;
import java.nio.file.Path;
import java.util.Comparator;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import p4.aaa000.EstimateLoader;

/**
 * @author rfoy
 *
 */
public class RailPlanner {
	
	private static class PrimTracker implements Comparable<PrimTracker>{

		private static final Comparator<PrimTracker> TRACKER_COMPARATOR = Comparator.comparingInt(PrimTracker::getKey).thenComparing(PrimTracker::getNode);
		private int key;
		private String parent;
		private String node;

		public String getNode() {
			return node;
		}

		public int getKey() {
			return key;
		}

		public PrimTracker(String node) {
			this.node = node;
			this.key = Integer.MAX_VALUE;
			this.parent = null;
		}

		@Override
		public String toString() {
			return String.format("[%s :: %d (%s)]", node, key, parent);
		}

		@Override
		public int compareTo(PrimTracker o) {
			return TRACKER_COMPARATOR.compare(this, o);
		}
	}

	/**
	 * Returns the lowest cost RailPlan using Prim's Algorithm
	 * @param scenarioSuffix absolute path to an estimate file
	 * @return a graph representing the lowest-cost route that connects all cities in estimate file
	 */
	public static ValueGraph<String, Integer> createPlan(String filePath){
		var estimates = EstimateLoader.loadEstimate(filePath);
		return  findMinimumSpanningTree(estimates);
	}

	private static ValueGraph<String, Integer> findMinimumSpanningTree(ValueGraph<String, Integer> estimates) {
		Map<String, PrimTracker> tracker = new HashMap<>();
		String startNode = findStartNode(estimates);
		initialize(tracker, estimates, startNode);
		PriorityQueue<PrimTracker> pq = new PriorityQueue<>(tracker.values());
		
		while (!pq.isEmpty()) {
			String u = pq.remove().node;
			for (String v : estimates.adjacentNodes(u)) {
				PrimTracker vTracker = tracker.get(v);
				Integer uvCost = estimates.edgeValue(u, v).get();
				if (pq.contains(vTracker) && uvCost < vTracker.key) {
					pq.remove(vTracker);
					vTracker.parent = u;
					vTracker.key = uvCost;
					pq.add(vTracker);
				}
			}
		}
		
		MutableValueGraph<String, Integer> result = ValueGraphBuilder.undirected().build();

		for (var value : tracker.values()) {
			if (value.parent != null) {
				result.putEdgeValue(value.node, value.parent, value.key);
			}
		}
		
		return ImmutableValueGraph.copyOf(result);
	}

	private static String findStartNode(ValueGraph<String, Integer> estimates) {
		EndpointPair<String> minEdgeSoFar = null;
		for(var edge : estimates.edges()) {
			if (minEdgeSoFar == null || estimates.edgeValue(minEdgeSoFar).get() > estimates.edgeValue(edge).get()) {
				minEdgeSoFar = edge;
			}
		}
		return minEdgeSoFar.nodeU();
	}

	private static void initialize(Map<String, PrimTracker> tracker, ValueGraph<String, Integer> estimates, String startNode) {
		for (String node : estimates.nodes()) {
			tracker.put(node, new PrimTracker(node));
		}
		tracker.get(startNode).key = 0;
	}
}
