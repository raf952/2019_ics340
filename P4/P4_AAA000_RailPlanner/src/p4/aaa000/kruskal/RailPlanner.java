package p4.aaa000.kruskal;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import p4.aaa000.EstimateLoader;

public class RailPlanner {

	static class EdgeComparator implements Comparator<EndpointPair<String>> {
		private ValueGraph<String, Integer> estimateGraph;

		public EdgeComparator(ValueGraph<String, Integer> estimateGraph) {
			this.estimateGraph = estimateGraph;
		}

		@Override
		public int compare(EndpointPair<String> o1, EndpointPair<String> o2) {
			return Integer.compare(estimateGraph.edgeValue(o1).get(), estimateGraph.edgeValue(o2).get());
		}
	}

	public static ValueGraph<String, Integer> createPlan(String estimateFilePath) {

		ValueGraph<String, Integer> estimateGraph = EstimateLoader.loadEstimate(estimateFilePath);
		return ImmutableValueGraph.copyOf(findMinimumSpanningTree(estimateGraph));
	}

	private static ValueGraph<String, Integer> findMinimumSpanningTree(ValueGraph<String, Integer> estimateGraph) {
		MutableValueGraph<String, Integer> result = ValueGraphBuilder.undirected().build();

		Map<String, Set<String>> tracker = new HashMap<>();
		initialize(tracker, estimateGraph);
		PriorityQueue<EndpointPair<String>> pq = new PriorityQueue<>(new EdgeComparator(estimateGraph));
		pq.addAll(estimateGraph.edges());
		while (!pq.isEmpty()) {
			var currEdge = pq.remove();
			final Set<String> nodeUset = tracker.get(currEdge.nodeU());
			final Set<String> nodeVset = tracker.get(currEdge.nodeV());
			if (!nodeUset.equals(nodeVset)) {
				result.putEdgeValue(currEdge, estimateGraph.edgeValue(currEdge).get());
				union(tracker, nodeUset, nodeVset);
			}
		}

		return result;
	}

	private static void union(Map<String, Set<String>> tracker, Set<String> nodeUset, Set<String> nodeVset) {
		var union = new HashSet<String>(nodeUset);
		union.addAll(nodeVset);
		for (var node : union) {
			tracker.put(node, union);
		}
	}

	private static void initialize(Map<String, Set<String>> tracker, ValueGraph<String, Integer> estimateGraph) {
		for (String node : estimateGraph.nodes()) {
			final HashSet<String> set = new HashSet<String>();
			set.add(node);
			tracker.put(node, set);
		}
	}
}
