package p4.aaa000;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Logger;

import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import p4.aaa000.kruskal.RailPlanner;

/**
 * @author rfoy
 *
 */
public class EstimateLoader {

	/**
	 * Loads an estimate file per P4 Specification
	 * @param estimateFilePath absolute path to estimate file
	 * @return Graph of estimates
	 */
	public static ValueGraph<String, Integer> loadEstimate(String estimateFilePath) {
		MutableValueGraph<String, Integer> estimateGraph = ValueGraphBuilder.undirected().build();
		try (Scanner scn = new Scanner(new File(estimateFilePath));) {
			scn.useDelimiter("[\\|\n]");
			while (scn.hasNext()) {
				estimateGraph.putEdgeValue(scn.next(), scn.next(), scn.nextInt());
			}
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
		return ImmutableValueGraph.copyOf(estimateGraph);
	}

}
