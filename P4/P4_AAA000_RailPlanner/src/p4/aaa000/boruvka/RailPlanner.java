package p4.aaa000.boruvka;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import p4.aaa000.EstimateLoader;

/**
 * @author rfoy
 *
 */
public class RailPlanner {

	private static class BoruvkaComponent {
		/*
		 * @Override public String toString() { return String.format("%s (%s:%d)",
		 * nodes, cheapestEdge, minValue); }
		 */

		private int minValue;

		@Override
		public String toString() {
			return String.format("[%d, %s, %s]", minValue, tree.nodes(), cheapestEdge);
		}

		private Graph<String> tree;
		private EndpointPair<String> cheapestEdge;

		public BoruvkaComponent(Graph<String> tree) {
			this.minValue = Integer.MAX_VALUE;
			this.tree = tree;
		}

		public void setCheapestEdge(EndpointPair<String> edge, int value) {
			this.cheapestEdge = edge;
			this.minValue = value;
		}

	}

	/**
	 * @param estimateFilePath
	 * @return
	 */
	public static ValueGraph<String, Integer> createPlan(String estimateFilePath) {
		var estimates = EstimateLoader.loadEstimate(estimateFilePath);
		return findMinimumSpanningTree(estimates);

	}

	@SuppressWarnings("unchecked")
	private static ValueGraph<String, Integer> findMinimumSpanningTree(ValueGraph<String, Integer> estimates) {
		Map<Graph<String>, BoruvkaComponent> treeTracker = new HashMap<>();
		Map<String, Graph<String>> nodeTracker = new HashMap<>();
		Set<Graph<String>> forest = getComponents(estimates);
		boolean addedEdge;

		do {
			addedEdge = false;
			clearCheapestEdges(treeTracker, forest);
			update(nodeTracker, forest);
			findConnectingEdges(estimates, treeTracker, nodeTracker);
			addedEdge = connectComponents(treeTracker, nodeTracker, forest);
		} while ((forest.size() > 1) && addedEdge);
		if (!addedEdge) {
			throw new IllegalArgumentException("Graph is not fully connected");
		}
		return createValuegraph((Graph<String>) forest.toArray()[0], estimates);
	}

	/**
	 * @param treeTracker
	 * @param nodeTracker
	 * @param forest
	 * @param addedEdge
	 * @return
	 */
	private static boolean connectComponents(Map<Graph<String>, BoruvkaComponent> treeTracker,
			Map<String, Graph<String>> nodeTracker, Set<Graph<String>> forest) {
		boolean addedEdge = false;
		for (var component : treeTracker.values()) {
			if (component.cheapestEdge != null) {
				addCheapestEdge(nodeTracker, component, forest);
				addedEdge = true;
			}
		}
		return addedEdge;
	}

	/**
	 * @param estimates
	 * @param treeTracker
	 * @param nodeTracker
	 */
	private static void findConnectingEdges(ValueGraph<String, Integer> estimates,
			Map<Graph<String>, BoruvkaComponent> treeTracker, Map<String, Graph<String>> nodeTracker) {
		for (var edge : estimates.edges()) {
			BoruvkaComponent uComponent = treeTracker.get(nodeTracker.get(edge.nodeU()));
			BoruvkaComponent vComponent = treeTracker.get(nodeTracker.get(edge.nodeV()));
			// TODO fix logic with new trackers
			if (!uComponent.tree.equals(vComponent.tree)) {
				Integer uvValue = estimates.edgeValue(edge).get();
				if (uvValue < uComponent.minValue) {
					uComponent.setCheapestEdge(edge, uvValue);
				}
				if (uvValue < vComponent.minValue) {
					vComponent.setCheapestEdge(edge, uvValue);
				}
			}
		}
	}

	private static ValueGraph<String, Integer> createValuegraph(Graph<String> mst,
			ValueGraph<String, Integer> estimates) {
		MutableValueGraph<String, Integer> result = ValueGraphBuilder.undirected().build();

		for (var edge : mst.edges()) {
			result.putEdgeValue(edge, estimates.edgeValue(edge).get());
		}
		return ImmutableValueGraph.copyOf(result);
	}

	private static Set<Graph<String>> getComponents(ValueGraph<String, Integer> estimates) {
		Set<Graph<String>> components = new HashSet<>();
		for (var node : estimates.nodes()) {
			MutableGraph<String> tree = GraphBuilder.undirected().build();
			tree.addNode(node);
			components.add(tree);
		}
		return components;
	}

	private static void addCheapestEdge(Map<String, Graph<String>> nodeTracker, BoruvkaComponent component,
			Set<Graph<String>> forest) {

		var treeA = component.tree;
		var treeB = nodeTracker.get(component.cheapestEdge.nodeU());
		if (treeB == treeA) {
			treeB = nodeTracker.get(component.cheapestEdge.nodeV());
		}
		
		MutableGraph<String> newTree = GraphBuilder.undirected().build();

		for (var edge : treeA.edges()) {
			newTree.putEdge(edge);
		}
		for (var node : treeA.nodes()) {
			newTree.addNode(node);
		}

		for (var edge : treeB.edges()) {
			newTree.putEdge(edge);
		}
		for (var node : treeB.nodes()) {
			newTree.addNode(node);
		}

		newTree.putEdge(component.cheapestEdge);
		forest.remove(treeA);
		forest.remove(treeB);
		forest.add(newTree);
		update(nodeTracker, forest);
	}

	private static void clearCheapestEdges(Map<Graph<String>, BoruvkaComponent> treeTracker,
			Set<Graph<String>> forest) {
		treeTracker.clear();
		for (var tree : forest) {
			var component = new BoruvkaComponent(tree);
			component.setCheapestEdge(null, Integer.MAX_VALUE);
			treeTracker.put(tree, component);
		}
	}

	private static void update(Map<String, Graph<String>> tracker, Set<Graph<String>> forest) {
		for (var tree : forest) {
			for (String node : tree.nodes()) {
				tracker.put(node, tree);
			}
		}

	}
}
